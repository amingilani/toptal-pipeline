# frozen_string_literal: true

require 'rails_helper'

RSpec.describe PostsController, type: :controller do
  it 'should respond to index' do
    get :index
    expect(response).to have_http_status(200)
  end
end
